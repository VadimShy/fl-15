class BirthdayService {
    
    constructor(date) {
        this.date = date;
    }
   
    howLongToMyBirthday() {
          
        const oneDay = 24*60*60*1000;
        const secondDate = new Date(new Date().getFullYear()+1,3,5);
        const date = new Date();
        console.log(Math.round(Math.abs((date.getTime() - secondDate.getTime())/oneDay)));
    }
}

BirthdayService.howLongToMyBirthday()