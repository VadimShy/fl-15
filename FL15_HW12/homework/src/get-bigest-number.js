function getBiggestNumber(...args) {
    const {length} = args;
    args.forEach(i => {
        if (typeof i !== 'number') {
            throw 'Wrong argument type'
        }
    })
    if (length < 2) {
        return 'Not enough arguments'
    }
    if (length > 10) {
        return 'Too many arguments'
    }

    return Math.max(...args)
}

module.exports = getBiggestNumber;