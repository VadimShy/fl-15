const formVisible = document.getElementById('form');
const formHidden = document.getElementById('form');

const timeInputValidation = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;
const toFixedDigit = 2;

let eventName = prompt('Please type Event Name', 'meeting');

eventName ? formVisible.style.visibility='visible' : formHidden.style.visibility='hidden';

function showMeetingDetails() {
    const userEnterName = document.getElementById('name').value;
    const userEnterTime = document.getElementById('time').value;
    const userEnterPlace = document.getElementById('place').value;
    
    if (userEnterName && timeInputValidation.test(userEnterTime) && userEnterPlace) {
        
        console.log(`${userEnterName} has a meeting today at ${userEnterTime} somewhere in ${userEnterPlace}`);
    }else if (timeInputValidation.test(userEnterTime) === false) {

        alert('Enter time in format hh:mm');
    }else {

        alert('Input all data');
    }
} 

function showCurrencyConverter() {
    
    const amountOfEuro = prompt('Input amount of Euro', '100');
    const amountOfDollar = prompt('Input amount of Dollar', '100');
    const dollarExchangeRate = 27.72;
    const euroExchangeRate = 32.93;

    if (amountOfEuro >= 0 && amountOfDollar >= 0) {

        alert(`${amountOfEuro} euros are equal ${(amountOfEuro*euroExchangeRate).toFixed(toFixedDigit)}hrns, 
    ${amountOfDollar} dollars are equal ${(amountOfDollar*dollarExchangeRate).toFixed(toFixedDigit)}hrns `)
    }else {
        alert('Wrong Input: only positive numbers')
    }
}