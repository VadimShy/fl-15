function reverseNumber(num) {
    
    let digit;
    let reverse = 0;
    let divider = 10;
    
    while(num) {
        digit = num % divider;
        reverse = reverse * divider + digit;
        num = num / divider | 0;
    }

    return reverse;
}

function forEach(arr, func) {
    let i;
        
    for (i = 0; i < arr.length; i++) {
        func(arr[i]);
    }

}

function map(arr, func) {
    let arr2 = [];
  
    forEach(arr, el => arr2.push(func(el)));

  return arr2;

}

function filter(arr, func) {
    let filteredAray = [];
  
    forEach(arr, el => { 
    if (func(el)) {
      filteredAray.push(el);
    }
  });
      
  return filteredAray;

}

function getKeys(obj) {
    let arr = [];
    
    for (let key in obj) {
        if (arguments[0].hasOwnProperty(key)) {
            arr.push(key);
        }
    }

    return arr;
}

function getValues(obj) {
    let arr = [];

    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
        arr.push(obj[key]);
        }
    }
    return arr;

}

function showFormattedDate(dateObj) {
    let dateInMilliSec = Date.parse(dateObj);
    let dateTransform = new Date(dateInMilliSec);
    let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    
    return `It is ${dateTransform.getDate()} of ${months[dateTransform.getMonth()]}, ${dateTransform.getFullYear()}`

}



